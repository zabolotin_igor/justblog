﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JustBlogProject.EntityFramework;
using JustBlogProject.Models;
using JustBlogProject.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace JustBlogProject.Services
{
    public class HomeService : IHomeService
    {
        private readonly JustBlogDbContext context;
        private readonly HttpContext httpContext;
        IHostingEnvironment _appEnvironment;

        public HomeService(JustBlogDbContext db, IHttpContextAccessor httpContextAccessor, IHostingEnvironment appEnvironment)
        {
            this.context = db;
            this.httpContext = httpContextAccessor.HttpContext;
            this._appEnvironment = appEnvironment;
        }

        public async Task AddPost(PostModel postModel)
        {
            var post = new Post();
            post.title = postModel.title;
            post.text = postModel.text;
            if (postModel.image != null)
            {
                // путь к папке Files
                string path = "/Files/" + postModel.image.FileName;
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await postModel.image.CopyToAsync(fileStream);
                }
                Models.File file = new Models.File { Name = postModel.image.FileName, Path = path };
                context.Files.Add(file);
                post.file = file;

            }
            if (postModel.text.Length > 200)
            {
                post.cropedText = postModel.text.Remove(200);
            }
            else
            {
                post.cropedText = postModel.text;
            }
            post.postTime = DateTime.Now;
            post.isChecked = true;//TODO: moderation
            post.user= context.Users.FirstOrDefault(u => u.username == httpContext.User.Identity.Name);

            var category = new Category();
            category.name = postModel.category;

            var postCategory = new CategoryPost();
            postCategory.Category = category;
            postCategory.Post = post;

            await context.Posts.AddAsync(post);
            await context.CategoryPosts.AddAsync(postCategory);
            context.SaveChanges();

        }

        public Task EditUserInfo(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Post>> GetHotPosts()
        {
            throw new NotImplementedException();
        }

        public async Task<Post> likePost(int? id)
        {
            if (id != null)
            {
                var post = await context.Posts.Where(p => p.id == id).FirstOrDefaultAsync();
                post.like = post.like + 1;
                context.SaveChanges();
                return post;
            }
            return null;
        }

        public Task<List<Post>> GetLatestPosts()
        {
             var posts =  context.Posts.Include(file=>file.file)
                .Include(p=>p.user).
                ThenInclude(s=>s.profile).
                ToListAsync();
             return posts;
        }

        public Task<List<Post>> GetUserPosts(int id)
        {
            var posts = context.Posts.Where(p => p.user.id == id).Include(file => file.file).Include(p => p.user).
                ThenInclude(s => s.profile).ToListAsync();
            return posts;
        }

        public async Task Logout()
        {
            await this.httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}
