﻿using JustBlogProject.Models;
using JustBlogProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Services
{
    public interface IAccountService
    {
        Task<User> Register(RegisterModel userReq); 

        Task<User> Login(string username,string password);

        Task Logout();
    }
}
