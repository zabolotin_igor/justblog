﻿using JustBlogProject.Models;
using JustBlogProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Services
{
    public interface IHomeService
    {
        Task<List<Post>> GetLatestPosts();

        Task<List<Post>> GetHotPosts();

        Task<List<Post>> GetUserPosts(int id);

        Task AddPost(PostModel postModel);

        Task EditUserInfo(int id);

        Task<Post> likePost(int? id);

        Task Logout();
    }
}
