﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using JustBlogProject.EntityFramework;
using JustBlogProject.Extensions;
using JustBlogProject.Models;
using JustBlogProject.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace JustBlogProject.Services
{
    public class AccountService : IAccountService
    {
        private readonly JustBlogDbContext context;
        private readonly HttpContext httpContext;

        public AccountService(JustBlogDbContext db,IHttpContextAccessor httpContextAccessor)
        {
            this.context = db;
            this.httpContext = httpContextAccessor.HttpContext;
            InitRolesAndAdmin();

        }

        private void InitRolesAndAdmin()
        {
            if (!context.Roles.Any())
            {
                var adminRole=new Role {name="Admin" };
                var userRole=new Role {name="User" };
                var moderatorRole=new Role {name="Moderator" };
                context.Roles.Add(adminRole);
                context.Roles.Add(userRole);
                context.Roles.Add(moderatorRole);

                var admin = new User
                {
                    username = "GeneralAdmin",
                    role = adminRole
                };
                var adminProfile = new Profile
                {
                    name = "Admin",
                    surname = "Adminskiy",
                    email = "iqordetroit992@gmail.com"

                };
                admin.profile = adminProfile;
                (admin.passwordHash, admin.solt) = "admin".EncryptWithSalt();
                context.Users.Add(admin);
                context.SaveChanges();
            }
        }

        public async Task<User> Login(string username, string password)
        {
            Guard.Against.NullOrWhiteSpace(username, nameof(username));
            Guard.Against.NullOrWhiteSpace(password, nameof(password));

            var user = await this.context.Users.Include(u => u.role).FirstOrDefaultAsync(u => u.username == username);
            if (user != null)
            {
                var pass = password.Sha256(user.solt);
                if (pass == user.passwordHash)
                {
                    await Authenticate(user);
                    return user;
                }
            }
            return null;
        }

        public async Task Logout()
        {
            await this.httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<User> Register(RegisterModel userReq)
        {
            Guard.Against.NullOrWhiteSpace(userReq.username, nameof(userReq.username));
            Guard.Against.NullOrWhiteSpace(userReq.password, nameof(userReq.password));


            var user = await this.context.Users.FirstOrDefaultAsync(u => u.username == userReq.username);
            if (user == null)
            {
                user = new User
                {
                    username = userReq.username
                };
                (user.passwordHash, user.solt) = userReq.password.EncryptWithSalt();

                var userProfile = new Profile
                {
                    name = userReq.name,
                    surname = userReq.surname,
                    fathersName = userReq.fathersName,
                    email = userReq.email,

                };
                user.profile = userProfile;
                var role= context.Roles.FirstOrDefault(o => o.name == "User");
                user.role = role;
                this.context.Users.Add(user);
                await this.context.SaveChangesAsync();
                await Authenticate(user);
                return user;
            }
            return null;
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim> {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.username),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.role?.name ?? "User")
            };
            var id = new ClaimsIdentity(claims,
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            var principal = new ClaimsPrincipal(id);
            await this.httpContext.SignInAsync("Cookies", principal);
        }

    }
}
