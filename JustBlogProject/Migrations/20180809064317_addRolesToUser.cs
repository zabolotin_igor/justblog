﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace JustBlogProject.Migrations
{
    public partial class addRolesToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "role",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "soltedPassword",
                table: "Users",
                newName: "passwordHash");

            migrationBuilder.AddColumn<int>(
                name: "roleid",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_roleid",
                table: "Users",
                column: "roleid");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Role_roleid",
                table: "Users",
                column: "roleid",
                principalTable: "Role",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Role_roleid",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Users_roleid",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "roleid",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "passwordHash",
                table: "Users",
                newName: "soltedPassword");

            migrationBuilder.AddColumn<string>(
                name: "role",
                table: "Users",
                nullable: true);
        }
    }
}
