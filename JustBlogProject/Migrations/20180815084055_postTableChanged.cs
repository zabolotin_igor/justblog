﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JustBlogProject.Migrations
{
    public partial class postTableChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "photoPath",
                table: "Posts");

            migrationBuilder.AddColumn<byte[]>(
                name: "postPhoto",
                table: "Posts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "postPhoto",
                table: "Posts");

            migrationBuilder.AddColumn<string>(
                name: "photoPath",
                table: "Posts",
                nullable: true);
        }
    }
}
