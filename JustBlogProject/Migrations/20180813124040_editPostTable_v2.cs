﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JustBlogProject.Migrations
{
    public partial class editPostTable_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "photoPath",
                table: "Posts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "photoPath",
                table: "Posts");
        }
    }
}
