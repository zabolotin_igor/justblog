﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JustBlogProject.Migrations
{
    public partial class editPostTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "token",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "cropedText",
                table: "Posts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "like",
                table: "Posts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "postTime",
                table: "Posts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cropedText",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "like",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "postTime",
                table: "Posts");

            migrationBuilder.AddColumn<string>(
                name: "token",
                table: "Users",
                nullable: true);
        }
    }
}
