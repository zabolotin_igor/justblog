﻿using JustBlogProject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.EntityFramework
{
    public class JustBlogDbContext:DbContext
    {
        public JustBlogDbContext(DbContextOptions<JustBlogDbContext> options) : base(options) { }

        public virtual DbSet<Category> Categories { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }

        public virtual DbSet<Post> Posts { get; set; }

        public virtual DbSet<Profile> Profiles { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<File> Files { get; set; }

        public virtual DbSet<Role> Roles { get; set; }

        public virtual DbSet<CategoryPost> CategoryPosts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryPost>()
                .HasKey(bc => new { bc.PostId, bc.CategoryId });

            modelBuilder.Entity<CategoryPost>()
                .HasOne(bc => bc.Post)
                .WithMany(b => b.postCategory)
                .HasForeignKey(bc => bc.PostId);

            modelBuilder.Entity<CategoryPost>()
                .HasOne(bc => bc.Category)
                .WithMany(c => c.categoryPost)
                .HasForeignKey(bc => bc.CategoryId);

            modelBuilder.Entity<User>()
                .HasOne(a => a.profile)
                .WithOne(b => b.user)
                .HasForeignKey<Profile>(b => b.userRef);

            modelBuilder.Entity<Post>()
                .HasOne(a => a.file)
                .WithOne(b => b.Post)
                .HasForeignKey<File>(b => b.postRef);

        }
    }
}
