﻿using JustBlogProject.Services;
using JustBlogProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromForm]LoginModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.accountService.Login(model.username, model.password);
                if (user != null)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([FromForm]RegisterModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.accountService.Register(model);
                if (user != null)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }



        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await this.accountService.Logout();
            return RedirectToAction("Login", "Account");
        }
    }
}
