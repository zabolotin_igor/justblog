﻿using JustBlogProject.EntityFramework;
using JustBlogProject.Models;
using JustBlogProject.Services;
using JustBlogProject.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Controllers
{
    [Authorize]
    public class HomeController: Controller
    {
        private readonly JustBlogDbContext context;//TODO: Выпелить нахрен отсюда
        private readonly IHomeService homeService;

        public HomeController(IHomeService homeService,JustBlogDbContext context)
        {
            this.context = context;
            this.homeService = homeService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            
            var user = context.Users.Include(propfile=> propfile.profile).FirstOrDefault(u => u.username == User.Identity.Name);
            var profile = user.profile;
            ViewBag.name= profile.name;
            ViewBag.surname = profile.surname;
            ViewBag.email = profile.email;

            var post = new List<Post>();
                
            post= await homeService.GetLatestPosts();

            return View(post);
        }

        [HttpGet]
        public  IActionResult Create()
        {
            //TODO: something with this shit
            var user = context.Users.Include(propfile => propfile.profile).FirstOrDefault(u => u.username == User.Identity.Name);
            var profile = user.profile;
            ViewBag.name = profile.name;
            ViewBag.surname = profile.surname;
            ViewBag.email = profile.email;



  
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm]PostModel postModel)
        {
            if (this.ModelState.IsValid)
            {
                await homeService.AddPost(postModel);
                return RedirectToAction("Index", "Home");

            }
            return  View(postModel);
        }

        public IActionResult Logout()
        {
            homeService.Logout();
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> MyPosts()
        {

            var user = context.Users.Include(propfile => propfile.profile).FirstOrDefault(u => u.username == User.Identity.Name);
            var profile = user.profile;
            ViewBag.name = profile.name;
            ViewBag.surname = profile.surname;
            ViewBag.email = profile.email;

            var post = new List<Post>();

            post = await homeService.GetUserPosts(user.id);

            return View(post);
        }

        [HttpGet]
        public async Task<IActionResult> likePost(int? id)
        {
            if (id != null)
            {
                var post=await homeService.likePost(id);
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
