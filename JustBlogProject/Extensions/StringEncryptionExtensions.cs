﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.GuardClauses;

namespace JustBlogProject.Extensions
{
    public static class StringEncryptionExtensions
    {
        public static string Sha256(this string text, string salt)
        {
            Guard.Against.NullOrEmpty(text, nameof(text));
            Guard.Against.NullOrEmpty(salt, nameof(salt));

            using (var sha = System.Security.Cryptography.SHA256.Create())
            {
                var salted = Encoding.Default.GetBytes(text + salt);
                var bytes = sha.ComputeHash(salted);
                return Encoding.Default.GetString(bytes);
            }
        }
        public static (string password, string salt)
            EncryptWithSalt(this string text)
        { // extension method
            Guard.Against.NullOrEmpty(text, nameof(text));

            var saltBytes = new byte[32];
            var random = new Random();
            random.NextBytes(saltBytes);
            var salt = Encoding.Default.GetString(saltBytes);

            using (var sha = System.Security.Cryptography.SHA256.Create())
            {
                var salted = Encoding.Default.GetBytes(text + salt);
                var bytes = sha.ComputeHash(salted);
                return (Encoding.Default.GetString(bytes), salt);
            }
        }
    }
}
