﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.ViewModels
{
    public class RegisterModel
    {
        [Required]
        public string username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("password")]
        public string confirmPassword { get; set; }


        public string name { get; set; }

        public string surname { get; set; }

        public string fathersName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

    }
}
