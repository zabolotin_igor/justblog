﻿
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace JustBlogProject.ViewModels
{
    public class PostModel
    {
        [Required]
        public string title { get; set; }

        [Required]
        public string text { get; set; }

        public IFormFile image { get; set; }

        [Required]
        public string category { get; set; }

    }
}
