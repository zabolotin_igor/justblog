﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Models
{
    public class Comment
    {
        public int id { get; set; }

        public string text { get; set; }

        [Required]
        public User user { get; set; }
        [Required]
        public Post post { get; set; }
    }
}
