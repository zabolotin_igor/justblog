﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Models
{
    public class Profile
    {
        public int id { get; set; }

        public string name { get; set; }

        public string surname { get; set; }

        public string fathersName { get; set; }

        public string image { get; set; }

        public string email { get; set; }

        public string about { get; set; }

        public int userRef { get; set; }
        [Required]
        public User user { get; set; }
    }
}
