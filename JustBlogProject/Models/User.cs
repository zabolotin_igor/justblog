﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Models
{
    public class User
    {
        public int id { get; set; }

        public string username { get; set; }

        public string passwordHash { get; set; }

        public string solt { get; set; }

        public Role role { get; set; }

        public Profile profile { get; set; }

        public virtual ICollection<Post> posts { get; set; }

        public virtual ICollection<Comment> comments { get; set; }
    }
}
