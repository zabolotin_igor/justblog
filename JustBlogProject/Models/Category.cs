﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Models
{
    public class Category
    {
        public int id { get; set; }

        public string name { get; set; }

        public virtual ICollection<CategoryPost> categoryPost { get; set; }
    }
}
