﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlogProject.Models
{
    public class Post
    {
        public int id { get; set; }

        public DateTime postTime { get; set; }

        public string title { get; set; }

        public string cropedText { get; set; }

        public string text { get; set; }

        public bool isChecked { get; set; }

        public File file { get; set; }

        public int like { get; set; }
        [Required]
        public User user { get; set; }

        public virtual ICollection<CategoryPost> postCategory { get; set; }

        public virtual ICollection<Comment> comments { get; set; }

    }
}
